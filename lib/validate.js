const saIdParser = require("south-african-id-parser");
const moment = require("moment");

const getIdAge = (id) => {
  const { dateOfBirth } = saIdParser.parse(id);
  const birthyear = moment(dateOfBirth);
  const today = moment(new Date());
  return today.diff(birthyear, "years");
};

const getIdGender = (id) => {
  const isMale = saIdParser.parseIsMale(id);
  if (isMale) {
    return "M";
  }
  return "F";
};

const validate = (data) => {
  const { age, id, gender } = data;

  let validation = {};

  if (age) {
    const idAge = getIdAge(id);
    const ageIsValid = parseInt(idAge) === parseInt(age);
    const ageErrors = ageIsValid
      ? []
      : [`Provided age does not match ID`];
    validation["age"] = {
        received: age,
        expected: idAge,
        isValid: ageIsValid,
        errors: ageErrors,
    };
  }

  if (gender) {
    const idGender = getIdGender(id);
    
    const genderIsValid = idGender === gender;
    const genderErrors = genderIsValid
      ? []
      : [`Provided gender does not match ID`];
    validation["gender"] = {
      received: gender,
      expected: idGender,
      isValid: genderIsValid,
      errors: genderErrors,
    };
  }

  if (id) {
    const idValidation = saIdParser.parse(id);
    const isValid = idValidation.isValid;
    if (isValid === true) {
      validation["id"] = {
        received: id,
        isValid: true,
        errors: [],
      };
    } else {
      validation["id"] = {
        received: id,
        isValid: false,
        errors: [`Provided id is not valid`],
      };
    }
  }

  return validation;
};


module.exports = {
    validate,
};
